#!/bin/sh
# zzc -- z->z^2+c - Mandelbrot Music
# Copyright (c) 2010,2018,2023 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only
zzc="$(dirname "$(readlink -e "${0}")")"

# configuration
. "${zzc}/etc/zzc-player"

# prepare library
out="${zzc}/var/zzc"
mkdir -p "${out}" "${out}/j" "${out}/a"
[ -r "${out}/a/record" ] || echo 1 > "${out}/a/record"
[ -r "${out}/a/preset" ] || echo 0 > "${out}/a/preset"
[ -r "${out}/a/play" ] || echo 1 > "${out}/a/play"

# prepare temp
tmp="$(mktemp -d --tmpdir "zzc.XXXXXXXX")"
touch "${tmp}/running"

# render Julia sets
(
  mkdir -p "${tmp}/j"
  cd "${tmp}/j"
  while [ -e "${tmp}/running" ]
  do
    OMP_NUM_THREADS="${THREADS}" ZZC_JULIA="${RESOLUTION}" ZZC_PRESET="0" nice -n 15 "${zzc}/zzc" 2>/dev/null >zzc.txt
    nice -n 15 convert zzc.ppm zzc.png >/dev/null 2>/dev/null
    rm zzc.ppm
    mv zzc.png "${out}/j/$(cat zzc.txt | grep / | tr -d '\n' | tr ' /' '_-').png"
    rm zzc.txt
    # prune output
    while [ "$(du -b -s "${out}/j" | cut -f 1)" -gt "${IMAGESIZE}" ]
    do
      rm "$(find "${out}/j" -iname "*.png" | shuf -n 1)"
    done
    sync
  done
  touch "${tmp}/julia.done"
) &

# render audio
(
  mkdir -p "${tmp}/a"
  cd "${tmp}/a"
  record="$(cat "${out}/a/record")"
  preset="$(cat "${out}/a/preset")"
  presets="1 2 3 4"
  while [ -e "${tmp}/running" ]
  do
    if [ "$(du -b -s "${out}/a" | cut -f 1)" -gt "${AUDIOSIZE}" ]
    then
      # queue is full, wait for player to remove the head
      sleep 1
    else
      echo "1" > zzc.srt
      echo "00:00:00,000 --> 00:00:30,000" >> zzc.srt
      # the cp may fail in rare timing race vs julia pruning
      png="$(find  "${out}/j" -iname "*.png" | shuf -n 1)"
      cp "${png}" . &&
      angles="$(basename "${png%.png}" | tr '_-' ' /')" &&
      preset="$(echo "${presets}" | tr ' ' '\n' | grep -v "${preset}" | shuf -n 1)" &&
      ZZC_PRESET="${preset}" nice -n 10 "${zzc}/zzc" ${angles} 2>/dev/null > zzc.txt &&
      cat zzc.txt | grep -v bpm | grep -v @ | grep -v dB >> zzc.srt &&
      cat zzc.txt | grep dB >> "${out}/a/${preset}.log" &&
      rm zzc.txt &&
      nice -n 10 flac --totally-silent --best --verify --delete-input-file -w -T "ARTIST=ClaudiusMaximus" -T "COMPOSER=Claude Heiland-Allen" -T "ALBUM=z→z²+c" -T "TITLE=$(tail -n 2 zzc.srt | head -n 1): $(tail -n 1 zzc.srt)" -T "DATE=2023" -T "GENRE=Generative" -T "GENRE=Fractal" -T "LICENSE=Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License https://artlibre.org/licence/lal/en/" --picture "$(basename "${png}")" -o zzc.flac zzc.wav &&
      rm "$(basename "${png}")" &&
      mv zzc.srt "${out}/a/${record}.srt" &&
      mv zzc.flac "${out}/a/${record}.flac" &&
      record="$((record + 1))" &&
      echo "${record}" > "${out}/a/record" &&
      echo "${preset}" > "${out}/a/preset"
    fi
  done
  touch "${tmp}/record.done"
) &

# play
(
  mkdir -p "${tmp}/p"
  cd "${tmp}/p"
  play="$(cat "${out}/a/play")"
  while [ -e "${tmp}/running" ]
  do
    if [ -r "${out}/a/${play}.flac" ]
    then
      mpv --really-quiet --fs "${out}/a/${play}.flac"
      rm "${out}/a/${play}.flac"
      rm "${out}/a/${play}.srt"
      play="$((play + 1))"
      echo "${play}" > "${out}/a/play"
    else
      # record is falling behind
      sleep 1
    fi
  done
  touch "${tmp}/play.done"
) &

# signal stop
shutdown() {
  rm "${tmp}/running"
  echo "stopping..."
}
trap "shutdown" INT TERM

# cleanup
while [ -e "${tmp}/running" ]
do
  sleep 1
done
while [ ! -e "${tmp}/julia.done" ]
do
  sleep 1
done
while [ ! -e "${tmp}/record.done" ]
do
  sleep 1
done
while [ ! -e "${tmp}/play.done" ]
do
  sleep 1
done
rm -r "${tmp}"
