# z->z^2+c Installation Checklist

## Hardware

- Raspberry Pi 4 with 4GB RAM
- 32GB micro SD card

## OS

- arm64
- Debian Bookworm 2023-01-01 Raspberry Pi 4 image
- Debian Bullseye 2023-01-02 Raspberry Pi 4 image
- Raspberry Pi OS Lite Bullseye 2023-02-21

After imaging OS to micro SD card,
create a large partition at the end for data,
leaving 8GB for operating system,
and format it with f2fs.

First thing to do after installing is check if sound works over HDMI.

```
$ xzcat 20230101_raspi_4_bookworm.img.xz |
  sudo dd of=/dev/DEVICE bs=64k oflag=dsync status=progress
2621440000 bytes (2.6 GB, 2.4 GiB) copied, 346.339 s, 7.6 MB/s
$ sudo fdisk /dev/DEVICE
Command (m for help): p
Disk /dev/sdb: 29.5 GiB, 31674335232 bytes, 61863936 sectors
Disk model: CardReader      
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x117b328f

Device     Boot    Start      End  Sectors  Size Id Type
/dev/sdb1           8192  1048575  1040384  508M  c W95 FAT32 (LBA)
/dev/sdb2        1048576  5119999  4071424  1.9G 83 Linux
/dev/sdb3       16777216 61863935 45086720 21.5G 83 Linux

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.

$ sudo apt install f2fs-tools
$ sudo mkfs.f2fs -l zzc /dev/sdb3
$ sudo mount /dev/sdb2 /mnt/b/2/
$ sudo mkdir /mnt/b/2/var/zzc
$ sudo nano /mnt/b/2/etc/fstab
# The root file system has fs_passno=1 as per fstab(5) for automatic fsck.
LABEL=RASPIROOT / ext4 rw 0 1
# All other file systems have fs_passno=2 as per fstab(5) for automatic fsck.
LABEL=RASPIFIRM /boot/firmware vfat rw 0 2
LABEL=zzc /var/zzc f2fs rw 0 2
$ sudo umount /mnt/b/2
$ sync
$ sudo fdisk /dev/sdb
Command (m for help): p
Disk /dev/sdb: 29.5 GiB, 31674335232 bytes, 61863936 sectors
Disk model: CardReader      
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x117b328f

Device     Boot    Start      End  Sectors  Size Id Type
/dev/sdb1           8192  1048575  1040384  508M  c W95 FAT32 (LBA)
/dev/sdb2        1048576  5119999  4071424  1.9G 83 Linux
/dev/sdb3       16777216 61863935 45086720 21.5G 83 Linux

Command (m for help): d
Partition number (1-3, default 3): 2

Partition 2 has been deleted.

Command (m for help): n
Partition type
   p   primary (2 primary, 0 extended, 2 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (2,4, default 2): 2
First sector (2048-16777215, default 2048): 1048576
Last sector, +/-sectors or +/-size{K,M,G,T,P} (1048576-16777215, default 16777215): 

Created a new partition 2 of type 'Linux' and of size 7.5 GiB.
Partition #2 contains a ext4 signature.

Do you want to remove the signature? [Y]es/[N]o: n

Command (m for help): p

Disk /dev/sdb: 29.5 GiB, 31674335232 bytes, 61863936 sectors
Disk model: CardReader      
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x117b328f

Device     Boot    Start      End  Sectors  Size Id Type
/dev/sdb1           8192  1048575  1040384  508M  c W95 FAT32 (LBA)
/dev/sdb2        1048576 16777215 15728640  7.5G 83 Linux
/dev/sdb3       16777216 61863935 45086720 21.5G 83 Linux

Command (m for help): w

$ sudo resize2fs -p /dev/sdb2 
resize2fs 1.46.2 (28-Feb-2021)
Resizing the filesystem on /dev/sdb2 to 1966080 (4k) blocks.
The filesystem on /dev/sdb2 is now 1966080 (4k) blocks long.

```

## Users

- `claude` (artist)
- `zzc` (artwork)

both in audio and video groups, claude also in sudo group

- hostname `zzc`

## Packages

- git
- bash-completion
- lynx
- lightdm
- fluxbox
- libsndfile-dev
- libmpc-dev
- ffmpeg
- flac
- mpv
- imagemagick
- locales
- rsync
- sudo
- xfce4
- build-essential
- overlayroot (new in bookworm)

## Software

In zzc home:

```
mkdir -p opt/src
cd opt/src
git clone https://code.mathr.co.uk/mandelbrot-numerics.git
git clone https://code.mathr.co.uk/zzc.git
make -C mandelbrot-numerics/c/lib install
make -C zzc/src
```

configure `zzc/src/etc/zzc-player`

## Configuration

- set root password
- create users
- disable swap
- tmpfs for tmp (2GB)
- mount f2fs data parition on `/var/zzc`
- symlink `/var/zzc/a` to `/tmp/zzc/a`
- `/etc/security/limits.d/audio.conf`
- xorg configured with don't zap / don't zoom
- lightdm auto login zzc user to fluxbox session after 60 seconds
- fluxbox locked down (no menus / toolbars at all)
- fluxbox desktop background to zzc album cover
- mouse cursor invisible and moved to top right corner (repeatedly)
- killall light-locker
- xset s off
- xset -dpms
- xrandr --output XXX --mode 1920x1080 (?)
- streamer configured for 20GB images (1920x1080p64) 4 threads 1GB sounds
- config: `disable_overscan=1`
- config: `dtparam=sd_poll_once`
- config: `hdmi_group=1` CEA instead of DMT `hdmi_drive=2` HDMI instead of DVI; optional `hdmi_mode=1` force VGA
- set up ro root with rw temporary overlay
- duplicate / backup micro SD card

## Testing

- power on with screen unplugged -> does it work when screen is connected later
- power on with screen off -> does it work when screen is turned on later
- screen off/on while power on -> does it still work
- check what happens with screen higher than 1920x1080
- check what happens with screen lower than 1920x1080

## Display

- sRGB mode if exists
- adjust brightness / contrast to taste
