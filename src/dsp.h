// zzc -- z->z^2+c - Mandelbrot Music
// Copyright (c) 2010,2018,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

// dsp.h ported from clive, itself adapted from pd

#define pi 3.141592653589793
#define twopi 6.283185307179586

static inline sample wrap(sample x) {
  return x - floor(x);
}

static inline sample clamp(sample x, sample lo, sample hi)
{
  return lo < x ? x < hi ? x : hi : lo;
}

static inline sample mix(sample x, sample y, sample t) {
  return x + t * (y - x);
}

static inline sample mtof(sample f) {
  return (sample)8.17579891564 * exp((sample)0.0577622650 * fmin(f, (sample)1499));
}

// sample and hold

typedef struct {
  sample trigger;
  sample value;
} SAMPHOLD;

sample samphold(SAMPHOLD *s, sample value, sample trigger) {
  if (trigger < s->trigger) {
        s->value = value;
  }
  s->trigger = trigger;
  return s->value;
}

// based on pd's [vcf~] [lop~] [hip~]

typedef struct { double re, im; } VCF;

static inline sample vcf(VCF *s, sample x, sample hz, sample q) {
  double qinv = q > 0 ? 1 / q : 0;
  double ampcorrect = 2 - 2 / (q + 2);
  double cf = (double)hz * twopi / SR;
  if (cf < 0) { cf = 0; }
  double r = qinv > 0 ? 1 - cf * qinv : 0;
  if (r < 0) { r = 0; }
  double oneminusr = 1 - r;
  double cre = r * cos(cf);
  double cim = r * sin(cf);
  double re2 = s->re;
  s->re = ampcorrect * oneminusr * (double)x + cre * re2 - cim * s->im;
  s->im = cim * re2 + cre * s->im;
  return s->re;
}

typedef struct { double y; } LOP;

static inline sample lop(LOP *s, sample x, sample hz) {
  double c = clamp(twopi * (double) hz / SR, 0, 1);
  return s->y = mix(x, s->y, 1 - c);
}

typedef struct { double y; } HIP;

static inline sample hip(HIP *s, sample x, sample hz) {
  double c = clamp(1 - twopi * (double) hz / SR, 0, 1);
  double n = (1 + c) / 2;
  double y = (double) x + c * s->y;
  double o = n * (y - s->y);
  s->y = y;
  return o;
}

// pd/extra/hilbert~.pd

typedef struct { double w[2][2][2]; } HILBERT;

static inline void hilbert(sample out[2], HILBERT *h, const sample in[2]) {
  static const double c[2][2][5] =
    { { {  1.94632, -0.94657,   0.94657,  -1.94632, 1 }
      , {  0.83774, -0.06338,   0.06338,  -0.83774, 1 }
      }
    , { { -0.02569,  0.260502, -0.260502,  0.02569, 1 }
      , {  1.8685,  -0.870686,  0.870686, -1.8685,  1 }
      }
    };
  for (int i = 0; i < 2; ++i) {
    double w = (double) in[i] + c[i][0][0] * h->w[i][0][0] + c[i][0][1] * h->w[i][0][1];
    double x = c[i][0][2] * w + c[i][0][3] * h->w[i][0][0] + c[i][0][4] * h->w[i][0][1];
    h->w[i][0][1] = h->w[i][0][0];
    h->w[i][0][0] = w;
    w = x + c[i][1][0] * h->w[i][1][0] + c[i][1][1] * h->w[i][1][1];
    out[i] = c[i][1][2] * w + c[i][1][3] * h->w[i][1][0] + c[i][1][4] * h->w[i][1][1];
    h->w[i][1][1] = h->w[i][1][0];
    h->w[i][1][0] = w;
  }
}

// need accurate phase otherwise low frequency range is too quantized
typedef struct { double phase; } PHASOR;

static inline double phasor(PHASOR *p, double hz) {
  // can't use wrap as it is defined for sample which might be lower precision
  p->phase += hz / SR;
  p->phase -= floor(p->phase);
  return p->phase;
}

typedef struct { int length, woffset; } DELAY;

static inline void delwrite(DELAY *del, sample x0) {
  float *buffer = (float *) (del + 1);
  int l = del->length;
  l = (l > 0) ? l : 1;
  int w = del->woffset;
  buffer[w++] = x0;
  if (w >= l) { w -= l; }
  del->woffset = w;
}

static inline sample delread1(DELAY *del, sample ms) {
  float *buffer = (float *) (del + 1);
  int l = del->length;
  l = (l > 0) ? l : 1;
  int w = del->woffset;
  int d = ms / (sample)1000 * SR;
  d = (0 < d && d < l) ? d : 0;
  int r = w - d;
  r = r < 0 ? r + l : r;
  return buffer[r];
}
