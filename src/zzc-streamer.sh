#!/bin/sh
# zzc -- z->z^2+c - Mandelbrot Music
# Copyright (c) 2010,2018,2023 Claude Heiland-Allen
# SPDX-License-Identifier: AGPL-3.0-only
zzc="$(dirname "$(readlink -e "$0")")"
var="${zzc}/var"
mkdir -p "${var}/log"

# configuration
. "${zzc}/etc/zzc-streamer"

# prepare temp
tmp="$(mktemp -d --tmpdir "zzc.XXXXXXXX")"
touch "${tmp}/running"

# prepare queue
mkdir -p "${tmp}/a"
[ -r "${tmp}/a/record" ] || echo 1 > "${tmp}/a/record"
[ -r "${tmp}/a/preset" ] || echo 0 > "${tmp}/a/preset"
[ -r "${tmp}/a/play" ] || echo 1 > "${tmp}/a/play"

# stream
(
  while [ -e "${tmp}/running" ]
  do
    darkice -c "${zzc}/etc/darkice.cfg" &
    pid="$!"
    (
      while  [ -e "${tmp}/running" ]
      do
        sleep 10
      done
      kill "$pid"
    ) &
    wait "$pid"
    sleep 60
  done >> "${var}/log/darkice.log" 2>> "${var}/log/darkice.log"
  touch "${tmp}/stream.done"
) &

# record
(
  mkdir -p "${tmp}/a"
  cd "${tmp}/a"
  record="$(cat "${tmp}/a/record")"
  preset="$(cat "${tmp}/a/preset")"
  presets="1 2 3 4"
  while [ -e "${tmp}/running" ]
  do
    if [ "$(du -b -s "${tmp}/a" | cut -f 1)" -gt "${AUDIOSIZE}" ]
    then
      # queue is full, wait for player to remove the head
      sleep 10
    else
      preset="$(echo "${presets}" | tr ' ' '\n' | grep -v "${preset}" | shuf -n 1)" &&
      ZZC_PRESET="${preset}" nice -n 10 "${zzc}/zzc" 2>/dev/null > "${tmp}/a/${record}.txt" &&
      mv zzc.wav "${tmp}/a/${record}.wav" &&
      record="$((record + 1))" &&
      echo "${record}" > "${tmp}/a/record"
      echo "${preset}" > "${tmp}/a/preset"
    fi
  done
  touch "${tmp}/record.done"
) &

# play
(
  mkdir -p "${tmp}/p"
  cd "${tmp}/p"
  play="$(cat "${tmp}/a/play")"
  while [ -e "${tmp}/running" ]
  do
    if [ -r "${tmp}/a/${play}.wav" ]
    then
      track="$(cat "${tmp}/a/${play}.txt" | head -n 1 | tail -n 1)"
      tuning="$(cat "${tmp}/a/${play}.txt" | head -n 2 | tail -n 1)"
      angles="$(cat "${tmp}/a/${play}.txt" | head -n 3 | tail -n 1)"
      coords="$(cat "${tmp}/a/${play}.txt" | head -n 4 | tail -n 1)"
      peaks="$(cat "${tmp}/a/${play}.txt" | head -n 5 | tail -n 1)"
      echo "${peaks}" >> "${var}/log/${track}.log"
      curl -G -s -S --anyauth --netrc-file "${zzc}/etc/netrc" \
        --data-urlencode "mount=${mount}" \
        --data-urlencode "mode=updinfo" \
        --data-urlencode "charset=UTF-8" \
        --data-urlencode "song=${track}: ${angles} (${coords})" \
        "${protocol}${server}${port}/admin/metadata" > "${var}/log/curl.out" 2>> "${var}/log/curl.log"
      aplay -q "${tmp}/a/${play}.wav"  >> "${var}/log/aplay.out" 2>> "${var}/log/aplay.out"
      rm -f "${tmp}/a/${play}.wav" "${tmp}/a/${play}.txt"
      play="$((play + 1))"
      echo "${play}" > "${tmp}/a/play"
    else
      # record is falling behind
      sleep 10
    fi
  done
  touch "${tmp}/play.done"
) &

# signal stop
shutdown() {
  rm "${tmp}/running"
  echo "stopping..."
}
trap "shutdown" INT TERM

# cleanup
while [ -e "${tmp}/running" ]
do
  sleep 10
done
while [ ! -e "${tmp}/stream.done" ]
do
  sleep 1
done
while [ ! -e "${tmp}/record.done" ]
do
  sleep 1
done
while [ ! -e "${tmp}/play.done" ]
do
  sleep 1
done
rm -r "${tmp}"
