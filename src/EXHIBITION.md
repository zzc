# z->z^2+c

## Contact

Claude Heiland-Allen

## Hardware

### Provided by artist for duration of exhibition

- Raspberry Pi 4 in case, white and red.
- Raspberry Pi 4 power supply with UK plug, white, XXXcm.
- Mini HDMI (Raspberry Pi) to HDMI (screen) cable, white, XXXcm.
- 32GB Micro SD card installed with software (pre-inserted into Raspberry Pi 4).
- Self-addressed cardboard box with bubble wrap for return shipping if necessary.

### Provided by artist during setup and maintenance

- 4-way UK power strip.
- Laptop and power supply.
- Ethernet router and power supply.
- 2x ethernet cables.
- USB keyboard.
- USB mouse.
- Edirol UA-25 USB sound card and USB cable.
- 1/4" to 3.5mm headphone adaptor.
- 3.5mm earphones.
- Digital camera.

### Provided by gallery

- Video display with HDMI input, 1920x1080 resolution
- Sound output from HDMI, speakers (preferred) or headphones

## Setup

### Installation

1. Connect Rasperry Pi to screen via HDMI cable.
2. Power on screen.
3. Connect Raspberry Pi to power via power supply.

Raspberry Pi will boot into the installation software after a couple of minutes.

### Start

1. Power on screen.
2. Power on Raspberry Pi (plug in or switch on power).
3. Ensure correct video input is selected, fractal is displayed, and sound is operational.

It takes a couple of minutes for the Raspberry Pi to boot into the artwork.

### Stop

1. Power off screen.
2. Optional: power off Raspberry Pi (unplug or switch off power).

The Raspberry Pi can be left powered on for the duration of the exhibition.
