// zzc.c -- z->z^2+c - Mandelbrot Music
// Copyright (c) 2010,2018,2023 Claude Heiland-Allen
// SPDX-License-Identifier: AGPL-3.0-only

#include <tgmath.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sndfile.h>

#include <mandelbrot-numerics.h>

// single precision is safe here, because
// dsp.h uses double internally for recursive parts of filters
typedef float sample;
#define SR 44100
#include "dsp.h"
#define TABSIZE 4096 // Pd version uses 512

#ifndef M_PI
#define M_PI 3.14159265358979
#endif
#define plastic 1.32471795724475

#define MINDEPTH 2
#define MAXDEPTH 12
#define MINDEN 2
#define MAXDEN 24
#define MINPERIOD 360
#define MAXPERIOD (1 << MAXDEPTH)

#define PRECISE_JULIA 1
#define ITERS (1 << 20)
#define ER2 1e+9
#define IR2 1e-9

int gcd(int m, int n)
{
  m = m < 0 ? -m : m;
  n = n < 0 ? -n : n;
  if (m == 0 || n == 0) return 0;
  while (m != n)
  {
    if (m > n)
    {
      m -= n;
    }
    else
    {
      n -= m;
    }
  }
  return m;
}

void hsv2rgb(double h, double s, double v, double *o) {
  double i, f, p, q, t, r, g, b;
  int ii;
  if (s == 0.0) { r = g = b = v; } else {
    h = 6 * (h - floor(h));
    ii = i = floor(h);
    f = h - i;
    p = v * (1 - s);
    q = v * (1 - (s * f));
    t = v * (1 - (s * (1 - f)));
    switch(ii) {
      case 0: r = v; g = t; b = p; break;
      case 1: r = q; g = v; b = p; break;
      case 2: r = p; g = v; b = t; break;
      case 3: r = p; g = q; b = v; break;
      case 4: r = t; g = p; b = v; break;
      default:r = v; g = p; b = q; break;
    }
  }
  o[0] += r * r;
  o[1] += g * g;
  o[2] += b * b;
}

#ifdef PRECISE_JULIA
#define real double
#define Re creal
#define Im cimag
#else
#define real float
#define Re crealf
#define Im cimagf
#endif

uint64_t xorshift(uint64_t *a)
{
  uint64_t x = *a;
  x ^= x << 13;
  x ^= x >> 7;
  x ^= x << 17;
  *a = x;
  return x;
}

double uniform(uint64_t *a)
{
  return xorshift(a) / (double)(~(uint64_t)0);
}

// based on
// https://github.com/lycium/FractalTracer/blob/da1168615c3a0dfdb3208d0901952d4a2e0df758/src/renderer/Renderer.h#L108
double triangle(double v)
{
  const double orig = v * 2 - 1;
  if (orig == 0) return 0;
  v = orig / sqrt(fabs(orig));
  v = v - ((orig > 0) - (orig < 0));
  return v;
}

void j_d_render(unsigned char *ppm, int HEIGHT, int WIDTH, int SAMPLES, const real _Complex c, const real r, const int period)
{
  // real _Complex z0 = 0; // FIXME superattracting assumed below
  const real DR2 = r / HEIGHT * r / HEIGHT;
  const real ax = 1 / plastic;
  const real ay = ax / plastic;
  const real cx = Re(c);
  const real cy = Im(c);
  int64_t progress = 0;
  uint64_t prng = rand();
  prng += ! prng;
  for (int i = 0; i < 64; ++i)
  {
    xorshift(&prng);
  }
  int64_t skip = 0, total = 0;
  #pragma omp parallel for schedule(dynamic, 1)
  for (int j = 0; j < HEIGHT / 2; ++j)
  {
    uint64_t tprng = prng + j + 1;
    tprng += ! tprng;
    for (int i = 0; i < 64; ++i)
    {
      xorshift(&tprng);
    }
    for (int i = 0; i < WIDTH; ++i)
    {
      int me;
      {
        #pragma omp atomic capture
        me = progress++;
      }
      int pc = 100 * (me + 1) / (WIDTH * HEIGHT / 2);
      int pc_old = 100 * me / (WIDTH * HEIGHT / 2);
      if (pc != pc_old)
      {
        #pragma omp critical
        fprintf(stderr, "%3d%%\r", pc);
      }

      // calculate series approximation coefficients
      const real x0 = ((i+0.5) / WIDTH - (real)0.5) * WIDTH / HEIGHT * r;
      const real y0 = ((real)0.5 - (j+0.5) / HEIGHT) * r;
      real zx = x0;
      real zy = y0;
      real dzx = 1;
      real dzy = 0;
      real dzzx = 0;
      real dzzy = 0;
      int k = 0;
      for (k = 0; k < ITERS; ++k)
      {
        real dzzxn = 2 * (zx * dzzx - zy * dzzy + dzx * dzx - dzy * dzy);
        real dzzyn = 2 * (zx * dzzy + zy * dzzx + 2 * dzx * dzy);
        real dzxn = 2 * (zx * dzx - zy * dzy);
        real dzyn = 2 * (zx * dzy + zy * dzx);
        real dd2 = dzzxn * dzzxn + dzzyn * dzzyn;
        real d2 = dzxn * dzxn + dzyn * dzyn;
        if (d2 < dd2 * DR2)
        {
          break;
        }
        real zxn = zx * zx - zy * zy + cx;
        real zyn = 2 * zx * zy + cy;
        real z2 = zxn * zxn + zyn * zyn;
        if (z2 < IR2 || ER2 < z2) // FIXME when z0 is ever non-zero
        {
          break;
        }
        dzzx = dzzxn;
        dzzy = dzzyn;
        dzx = dzxn;
        dzy = dzyn;
        zx = zxn;
        zy = zyn;
      }

      if (0)
      {
        #pragma omp atomic
        skip += k;
      }
      const real jx = uniform(&tprng);
      const real jy = uniform(&tprng);
      double rgba[] = { 0, 0, 0, 0 };
      for (int s = 0; s < SAMPLES; ++s)
      {
        // low discrepancy sequence with triangle distribution
        const real bx = jx + s * ax;
        const real by = jy + s * ay;
        real dx = triangle(bx - floor(bx)) * r / HEIGHT;
        real dy = triangle(by - floor(by)) * r / HEIGHT;
        // initialize form series approximation
        real dwx = dzzx * dx - dzzy * dy;
        real dwy = dzzy * dx + dzzx * dy;
        real wx = zx + ((dzx + 0.5 * dwx) * dx - (dzy + 0.5 * dwy) * dy);
        real wy = zy + ((dzy + 0.5 * dwy) * dx + (dzx + 0.5 * dwx) * dy);
        dwx += dzx;
        dwy += dzy;
        dwx *= r / HEIGHT;
        dwy *= r / HEIGHT;
        real de = 0;
        int l = k;
        for (l = k; l < ITERS; ++l)
        {
          real dwxn = 2 * (wx * dwx - wy * dwy);
          real dwyn = 2 * (wx * dwy + wy * dwx);
          real wxn = wx * wx - wy * wy + cx;
          real wyn = 2 * wx * wy + cy;
          real w2 = wxn * wxn + wyn * wyn;
          dwx = dwxn;
          dwy = dwyn;
          wx = wxn;
          wy = wyn;
          if (w2 > (real) ER2)
          {
            de = (real)0.5 * log(w2) * sqrt(w2 / (dwx * dwx + dwy * dwy));
            real v = sqrt(tanh(sqrt(de)));
            hsv2rgb(0, 0, v, &rgba[0]);
            break;
          }
          // real _Complex w = w - z0; // FIXME if z0 is ever non-zero
          if (w2 < (real) IR2)
          {
            de = (real)0.5 * log(w2) * sqrt(w2 / (dwx * dwx + dwy * dwy));
            real v = sqrt(tanh(sqrt(-de)));
            hsv2rgb(((l % period) + 0.5) / period, 0.25, v, &rgba[0]);
            break;
          }
        }
        rgba[3] += 1;
        if (0)
        {
          #pragma omp atomic
          total += l;
        }
        // adaptive sampling
        if (fabs(de) > 8)
        {
          break;
        }
      }
      // 2x rotational symmetry about origin
      int ix = (j * WIDTH + i) * 3;
      ppm[ix++] = fmin(fmax(round(255 * sqrt(rgba[0] / rgba[3])), 0.), 255.);
      ppm[ix++] = fmin(fmax(round(255 * sqrt(rgba[1] / rgba[3])), 0.), 255.);
      ppm[ix++] = fmin(fmax(round(255 * sqrt(rgba[2] / rgba[3])), 0.), 255.);
      ix = ((HEIGHT - 1 - j) * WIDTH + (WIDTH - 1 - i)) * 3;
      ppm[ix++] = fmin(fmax(round(255 * sqrt(rgba[0] / rgba[3])), 0.), 255.);
      ppm[ix++] = fmin(fmax(round(255 * sqrt(rgba[1] / rgba[3])), 0.), 255.);
      ppm[ix++] = fmin(fmax(round(255 * sqrt(rgba[2] / rgba[3])), 0.), 255.);
    }
  }
  if (0)
  {
    fprintf(stderr, "skip = %f\n", skip / ((double) WIDTH * HEIGHT / 2));
    fprintf(stderr, "total = %f\n", total / ((double) WIDTH * HEIGHT / 2 * SAMPLES));
  }
}

// 1_osc~.pd

typedef struct
{
  int on;
  sample amp;
  sample hz;
  sample q;
  sample l, l_old;
  sample r, r_old;
  sample ramp;
  VCF ring;
  HIP dc;
  LOP rms;
} TheMilitaryIndustrialComplex1;

void TheMilitaryIndustrialComplex1_trigger(TheMilitaryIndustrialComplex1 *s, sample amp, sample hz, sample q, sample l, sample r)
{
  s->on = 1;
  s->l_old = s->l;
  s->r_old = s->r;
  s->amp = amp;
  s->hz = hz;
  s->q = q;
  s->l = l;
  s->r = r;
  s->ramp = 0;
}

void TheMilitaryIndustrialComplex1_go(TheMilitaryIndustrialComplex1 *s, sample *mixdown)
{
  if (s->on)
  {
    sample ms = (s->ramp += (sample)1000.0 / SR);
    sample x = ms < 5 ? 0 : ms < 6 ? mix(s->amp, 0, ms - 5) : 0;
    x = vcf(&s->ring, x, s->hz, s->q);
    x = tanh(x * x) * 2;
    x = hip(&s->dc, x * x, 10);
    mixdown[0] += x * (ms < 5 ? mix(s->l_old, 0, ms / 5) : ms < 15 ? 0 : ms < 30 ? mix(0, s->l, (ms - 15) / 15) : s->l);
    mixdown[1] += x * (ms < 5 ? mix(s->r_old, 0, ms / 5) : ms < 15 ? 0 : ms < 30 ? mix(0, s->r, (ms - 15) / 15) : s->r);
    sample vol = lop(&s->rms, x * x, 10);
    if (10 < ms && vol < (sample) 1e-6)
    {
      s->on = 0;
    }
  }
}

// 1_voice~.pd

#define MICVOICES 16

typedef struct
{
  int n;
  TheMilitaryIndustrialComplex1 o[MICVOICES];
  sample ms, decay;
  DELAY dl; float dlbuf[SR/10];
  DELAY dr; float drbuf[SR/10];
  HIP hi[2][2];
  LOP lo[2];
} TheMilitaryIndustrialComplex;

void TheMilitaryIndustrialComplex_trigger(TheMilitaryIndustrialComplex *s, double _Complex z, int v, sample note_offset)
{
  s->n++;
  s->n %= MICVOICES;
  s->dl.length = SR/10;
  s->dr.length = SR/10;

  const sample chord[MAXDEPTH] =
    { 24, 36, 43, 48
    , 51, 55, 60, 63
    , 67, 70, 72, 75
    };
  sample hz = mtof(chord[v] + note_offset);
  s->ms = 666 / hz;
  s->decay = expm1(-s->ms);
  sample x = creal(z);
  sample a = x * x;
  a *= a;
  sample b = x + (sample)1.3;
  sample q = b * 500 / hz;
  sample amp = a * b * q * 100 / sqrt(hz);
  //  * sqrt(1 + v); // this factor is not in the original Pd version
  sample y = cimag(z);
  y /= 3000;
  y *= 4000;
  y = wrap(y);
  y *= (sample) pi / 2;
  sample l = cos(y);
  sample r = sin(y);
  TheMilitaryIndustrialComplex1_trigger(&s->o[s->n], amp, hz, q, l, r);
}

void TheMilitaryIndustrialComplex_go(TheMilitaryIndustrialComplex *s, sample *mixdown)
{
  sample audio[2] = { 0, 0 };
  for (int i = 0; i < MICVOICES; ++i)
  {
    TheMilitaryIndustrialComplex1_go(&s->o[i], &audio[0]);
  }
  audio[0] = sin(audio[0]);
  audio[1] = sin(audio[1]);
  sample fb[2] =
    { delread1(&s->dr, s->ms) * s->decay
    , delread1(&s->dl, s->ms) * s->decay * -1
    };
  mixdown[0] += audio[0] + 2 * fb[0];
  mixdown[1] += audio[1] + 2 * fb[1];
  fb[0] = tanh(hip(&s->hi[0][0], lop(&s->lo[0], fb[0], 4000), 400));
  fb[1] = tanh(hip(&s->hi[0][1], lop(&s->lo[1], fb[1], 4000), 400));
  audio[0] = hip(&s->hi[1][0], audio[0], 1000);
  audio[1] = hip(&s->hi[1][1], audio[1], 1000);
  delwrite(&s->dl, audio[0] + fb[0]);
  delwrite(&s->dr, audio[1] + fb[1]);
}

// 2_osc~.pd

typedef struct
{
  int on;
  sample ramp;
  sample amp;
  sample hz;
  sample q;
  sample l, l_old;
  sample r, r_old;
  VCF ring;
  HILBERT hb;
  LOP rms;
} AfterTheDespoilment1;

void AfterTheDespoilment1_trigger(AfterTheDespoilment1 *s, sample amp, sample hz, sample q, sample l, sample r)
{
  s->on = 1;
  s->l_old = s->l;
  s->r_old = s->r;
  s->amp = amp;
  s->hz = hz;
  s->q = q;
  s->l = l;
  s->r = r;
  s->ramp = 0;
}

sample tabread2(const sample *t, sample x)
{
  int i = floor(x);
  x -= i;
  sample a = t[i];
  sample b = t[i + 1];
  return mix(a, b, x);
}

sample tabread4(const sample *buffer, sample x)
{
  int i = floor(x);
  sample t = x - i;
  sample y0 = buffer[i - 1];
  sample y1 = buffer[i    ];
  sample y2 = buffer[i + 1];
  sample y3 = buffer[i + 2];
  sample a0 = -t*t*t + 2*t*t - t;
  sample a1 = 3*t*t*t - 5*t*t + 2;
  sample a2 = -3*t*t*t + 4*t*t + t;
  sample a3 = t*t*t - t*t;
  return (a0 * y0 + a1 * y1 + a2 * y2 + a3 * y3) / 2;
}

void AfterTheDespoilment1_go(AfterTheDespoilment1 *s, sample *mixdown, sample *saw)
{
  if (s->on)
  {
    sample ms = (s->ramp += (sample)1000.0 / SR);
    sample x = ms < 5 ? 0 : ms < 6 ? mix(s->amp, 0, ms - 5) : 0;
    x = vcf(&s->ring, x, s->hz, s->q);
    sample h[2] = { x, x };
    hilbert(h, &s->hb, h);
    x = atan2(h[1], h[0]) / (sample)twopi + (sample)0.5;
    x *= TABSIZE;
    x += 1;
    x = tabread4(saw, x);
    x *= cbrt(h[0] * h[0] + h[1] * h[1]);
#if 1
    // Pd version
    mixdown[0] += x * (ms < 5 ? mix(s->l_old, 0, ms / 5) : ms < 10 ? mix(0, s->l, (ms - 5) / 5) : s->l);
    mixdown[1] += x * (ms < 5 ? mix(s->r_old, 0, ms / 5) : ms < 10 ? mix(0, s->r, (ms - 5) / 5) : s->r);
#else
    mixdown[0] += x * (ms < 10 ? mix(s->l_old, s->l, ms / 10) : s->l);
    mixdown[1] += x * (ms < 10 ? mix(s->r_old, s->r, ms / 10) : s->r);
#endif
    sample vol = lop(&s->rms, x * x, 10);
    if (10 < ms && vol < (sample) 1e-6)
    {
      s->on = 0;
    }
  }
}

// 2_voice~.pd

#define ATDVOICES 8

typedef struct
{
  int n;
  AfterTheDespoilment1 o[ATDVOICES];
} AfterTheDespoilment;

void AfterTheDespoilment_trigger(AfterTheDespoilment *s, double _Complex z, int v, sample note_offset)
{
  const int chord[MAXDEPTH] =
    { 24, 36, 43, 48
    , 51, 55, 60, 63
    , 67, 70, 72, 75
    };
  s->n++;
  s->n %= ATDVOICES;
  sample x = creal(z);
  sample a = x * x;
  a *= a;
  sample b = x + (sample)1.3;
  sample y = cimag(z);
  y /= 3000;
  sample hz = mtof(y / 48 + chord[v] + note_offset);
  sample c = b * 10000 / hz;
  b *= c * 100 / sqrt(hz);
  sample d = v + 1;
  d *= d;
  sample amp = a * b * d;
  sample q = c * d;
  y *= 4000;
  y = wrap(y);
  y *= (sample) pi / 2;
  sample l = cos(y);
  sample r = sin(y);
  AfterTheDespoilment1_trigger(&s->o[s->n], amp, hz, q, l, r);
}

void AfterTheDespoilment_go(AfterTheDespoilment *s, sample *mixdown, sample *saw)
{
  sample audio[2] = { 0, 0 };
  for (int i = 0; i < ATDVOICES; ++i)
  {
    AfterTheDespoilment1_go(&s->o[i], audio, saw);
  }
  mixdown[0] += sin(audio[0] / 3 * sqrt((sample) 8 / ATDVOICES)) / 4;
  mixdown[1] += sin(audio[1] / 3 * sqrt((sample) 8 / ATDVOICES)) / 4;
}

// 3_osc~.pd

typedef struct
{
  int on;
  sample ramp;
  sample amp;
  sample hz;
  sample q;
  sample l, l_old;
  sample r, r_old;
  VCF ring;
  HILBERT hb;
  LOP rms;
} RaceAgainstTime1;

void RaceAgainstTime1_trigger(RaceAgainstTime1 *s, sample amp, sample hz, sample q, sample l, sample r)
{
  s->on = 1;
  s->l_old = s->l;
  s->r_old = s->r;
  s->amp = amp;
  s->hz = hz;
  s->q = q;
  s->l = l;
  s->r = r;
  s->ramp = 0;
}

void RaceAgainstTime1_go(RaceAgainstTime1 *s, sample *mixdown, sample *sqr)
{
  if (s->on)
  {
    sample ms = (s->ramp += (sample)1000.0 / SR);
    sample x = ms < 5 ? 0 : ms < 6 ? mix(s->amp, 0, ms - 5) : 0;
    x = vcf(&s->ring, x, s->hz, s->q);
    sample h[2] = { x, x };
    hilbert(h, &s->hb, h);
    x = atan2(h[1], h[0]) / (sample)twopi + (sample)0.5;
    x *= TABSIZE;
    x += 1;
    x = tabread4(sqr, x);
    x *= pow((h[0] * h[0] + h[1] * h[1]) / 128, (sample)0.25);
    mixdown[0] += x * (ms < 5 ? mix(s->l_old, 0, ms / 5) : ms < 10 ? mix(0, s->l, (ms - 5) / 5) : s->l);
    mixdown[1] += x * (ms < 5 ? mix(s->r_old, 0, ms / 5) : ms < 10 ? mix(0, s->r, (ms - 5) / 5) : s->r);
    sample vol = lop(&s->rms, x * x, 10);
    if (10 < ms && vol < (sample) 1e-6)
    {
      s->on = 0;
    }
  }
}

// 3_voice~.pd

#define RATVOICES 12

typedef struct
{
  int n;
  RaceAgainstTime1 o[RATVOICES];
} RaceAgainstTime;

void RaceAgainstTime_trigger(RaceAgainstTime *s, double _Complex z, int v, sample note_offset)
{
  const int chord[MAXDEPTH] =
    { 26, 38, 50, 62
    , 74, 86, 98, 110
    , 33, 45, 57, 69
    };
  s->n++;
  s->n %= RATVOICES;
  sample x = creal(z);
  sample a = x * x;
  a *= a;
  sample b = x + (sample)1.3;
  sample y = cimag(z);
  y /= 3000;
  sample hz = mtof(y / 18 + chord[v] + note_offset);
  sample q = b * 10000 / hz;
  b *= q * 100 / sqrt(hz);
  sample amp = a * b;
  y *= 4000;
  y = wrap(y);
  y *= (sample) pi / 2;
  sample l = cos(y);
  sample r = sin(y);
  RaceAgainstTime1_trigger(&s->o[s->n], amp, hz, q, l, r);
}

void RaceAgainstTime_go(RaceAgainstTime *s, sample *mixdown, sample *sqr)
{
  sample audio[2] = { 0, 0 };
  for (int i = 0; i < RATVOICES; ++i)
  {
    RaceAgainstTime1_go(&s->o[i], audio, sqr);
  }
  mixdown[0] += sin(audio[0] * sqrt((sample) 12 / RATVOICES)) / 4;
  mixdown[1] += sin(audio[1] * sqrt((sample) 12 / RATVOICES)) / 4;
}

// 4_osc~.pd

typedef struct
{
  int on;
  sample ramp;
  sample amp;
  sample hz;
  sample q;
  sample l, l_old;
  sample r, r_old;
  VCF ring;
  HILBERT hb;
  LOP rms;
} BeforeCivilisation1;

void BeforeCivilisation1_trigger(BeforeCivilisation1 *s, sample amp, sample hz, sample q, sample l, sample r)
{
  s->on = 1;
  s->l_old = s->l;
  s->r_old = s->r;
  s->amp = amp;
  s->hz = hz;
  s->q = q;
  s->l = l;
  s->r = r;
  s->ramp = 0;
}

void BeforeCivilisation1_go(BeforeCivilisation1 *s, sample *mixdown, sample *saw)
{
  if (s->on)
  {
    sample ms = (s->ramp += (sample)1000.0 / SR);
    sample x = ms < 5 ? 0 : ms < 6 ? mix(s->amp, 0, ms - 5) : 0;
    x = vcf(&s->ring, x, s->hz, s->q);
    sample h[2] = { x, x };
    hilbert(h, &s->hb, h);
    x = atan2(h[1], h[0]) / (sample)twopi + (sample)0.5;
    x *= TABSIZE;
    x += 1;
    x = tabread4(saw, x);
    sample g = (h[0] * h[0] + h[1] * h[1]) / 128;
    g *= g;
    g = sin(clamp(g, 0, 2) * pi / 2); // not in Pd version, attempt to reduce occasional loud distortion
    x *= g;
    mixdown[0] += x * (ms < 5 ? mix(s->l_old, 0, ms / 5) : ms < 10 ? mix(0, s->l, (ms - 5) / 5) : s->l);
    mixdown[1] += x * (ms < 5 ? mix(s->r_old, 0, ms / 5) : ms < 10 ? mix(0, s->r, (ms - 5) / 5) : s->r);
    sample vol = lop(&s->rms, x * x, 10);
    if (10 < ms && vol < (sample) 1e-9)
    {
      s->on = 0;
    }
  }
}

// 4_voice~.pd

#define BCVOICES 16

typedef struct
{
  int n;
  BeforeCivilisation1 o[BCVOICES];
} BeforeCivilisation;

void BeforeCivilisation_trigger(BeforeCivilisation *s, double _Complex z, int v, sample note_offset)
{
  const int chord[MAXDEPTH] =
    { 31, 38, 43, 59
    , 62, 67, 74, 81
    , 28, 35, 40, 56
    };
  s->n++;
  s->n %= BCVOICES;
  sample x = creal(z);
  sample a = x * x;
  a *= a;
  sample b = x + (sample)1.3;
  sample y = cimag(z);
  y /= 3000;
  sample hz = mtof(y / 60 + chord[v] + note_offset);
  sample q = b * 10000 / hz;
  a *= q * 100 / sqrt(hz);
  q *= (v + 1) * (v + 1);
  sample amp = a * (v + 1);
  y *= 4000;
  y = wrap(y);
  y *= (sample) pi / 2;
  sample l = cos(y);
  sample r = sin(y);
  BeforeCivilisation1_trigger(&s->o[s->n], amp, hz, q, l, r);
}

void BeforeCivilisation_go(BeforeCivilisation *s, sample *mixdown, sample *saw)
{
  sample audio[2] = { 0, 0 };
  for (int i = 0; i < BCVOICES; ++i)
  {
    BeforeCivilisation1_go(&s->o[i], audio, saw);
  }
  mixdown[0] += sin(4 * audio[0] * sqrt((sample) 16 / BCVOICES));
  mixdown[1] += sin(4 * audio[1] * sqrt((sample) 16 / BCVOICES));
}

// main.pd / $0-guts / $0-sonification

typedef struct
{
  int reloaded;
  int preset;
  sample note_offset;
  sample saw[TABSIZE+3];
  sample sqr[TABSIZE+3];
  TheMilitaryIndustrialComplex v1[MAXDEPTH];
  AfterTheDespoilment          v2[MAXDEPTH];
  RaceAgainstTime              v3[MAXDEPTH];
  BeforeCivilisation           v4[MAXDEPTH];
  int depth;
  int num[MAXDEPTH];
  int den[MAXDEPTH];
  int period;
  double _Complex iters[MAXPERIOD];
  double _Complex seq[MAXDEPTH * MAXPERIOD];
  sample bpm;
  int beat;
  PHASOR phase;
  SAMPHOLD sh;
  LOP rms;
  sample peak;
  double sum0;
  double sum2;
} S;

int go(S *s, int outchannels, short *out)
{
  const double _Complex c0 = 0;
  const int p0 = 1;

  if (s->reloaded)
  {
    if (! (0 < s->depth && s->depth < MAXDEPTH))
    {
      fprintf(stderr, "error: bad internal angle count\n");
      return 1;
    }
    double _Complex z = 0;
    double _Complex c = c0;
    int p = p0;
    for (int i = 0; i < s->depth; ++i)
    {
      if (! (0 < s->num[i] && s->num[i] < s->den[i]))
      {
        fprintf(stderr, "error: bad internal angle\n");
        return 1;
      }
      double a = 2 * M_PI * s->num[i] / s->den[i];
      double _Complex t = cos(a) + I * sin(a);
      double _Complex c9 = c;
      m_d_interior(&z, &c9, 0, c9, 0.9 * t, p, 64);
      double _Complex c10 = c9;
      m_d_interior(&z, &c10, 0, c10, 1.0 * t, p, 64);
      double _Complex dc = (c10 - c9) / (1.0 - 0.9);
      int q = s->den[i];
      double _Complex c1qq = c10 + dc / (q * q);
      p *= q;
      m_d_nucleus(&c, c1qq, p, 64);
    }
    m_d_attractor(&z, 0, c, p / s->den[s->depth - 1], 64);
    double r = 10 * cabs(z);
    printf("%.18f + %.18f i @ %.5e P %d\n", creal(c), cimag(c), r, p);
    char *render_julia_set = getenv("ZZC_JULIA");
    if (render_julia_set)
    {
      int WIDTH = 0, HEIGHT = 0, SAMPLES = 0;
      if (3 == sscanf(render_julia_set, "%dx%dx%d", &WIDTH, &HEIGHT, &SAMPLES) && WIDTH > 0 && HEIGHT > 0 && SAMPLES > 0)
      {
        if ((HEIGHT & 1) || (WIDTH & 1))
        {
          fprintf(stderr, "error: dimensions must be even\n");
          return 1;
        }
        unsigned char *ppm = malloc(HEIGHT * WIDTH * 3);
        if (! ppm)
        {
          fprintf(stderr, "error: could not allocate memory\n");
          return 1;
        }
        j_d_render(ppm, HEIGHT, WIDTH, SAMPLES, c, r, p);
        FILE *o = fopen("zzc.ppm", "wb");
        if (! o)
        {
          free(ppm);
          fprintf(stderr, "error: could not open zzc.ppm for writing\n");
          return 1;
        }
        fprintf(o, "P6\n%d %d\n255\n", WIDTH, HEIGHT);
        fwrite(ppm, WIDTH * HEIGHT * 3, 1, o);
        fclose(o);
        free(ppm);
      }
      else
      {
        fprintf(stderr, "error: could not parse ZZC_JULIA dimensions WxHxS\n");
        return 1;
      }
    }
    s->period = p;
    if (! (s->period <= MAXPERIOD))
    {
      fprintf(stderr, "error: period too high (%d > %d)\n", s->period, MAXPERIOD);
      return 1;
    }

    if (! s->preset)
    {
      return 1;
    }

    // main.pd / $0-guts / $0-analysis / $0-mandelbrot-iteration

    z = 0;
    for (int i = 0; i < p; ++i)
    {
      s->iters[i] = z;
      z = z * z + c;
    }

    // main.pd / $0-guts / $0-sonify

    p = p0;
    for (int i = 0; i < s->depth; ++i)
    {
      const int q = s->period / p;
      for (int j = 0; j < q; ++j)
      {
        double _Complex mean = 0;
        for (int k = 0; k < p; ++k)
        {
          mean += s->iters[k * q + j];
        }
        mean /= q;
        for (int k = 0; k < p; ++k)
        {
          s->seq[i * s->period + k * q + j] = s->iters[k * q + j] - mean;
        }
        double rma = -1.0/0.0, rmi = 1.0/0.0, ima = -1.0/0.0, imi = 1.0/0.0;
        for (int k = 0; k < p; ++k)
        {
          double _Complex x = s->seq[i * s->period + k * q + j];
          rma = fmax(rma, creal(x));
          rmi = fmin(rmi, creal(x));
          ima = fmax(ima, cimag(x));
          imi = fmin(imi, cimag(x));
        }
        double sr = (rma - rmi) / 2;
        double si = (ima - imi) / 2;
        double sc = fmax(sr, si);
        sc += sc == 0.0;
        double _Complex o = ((rma + rmi) + I * (ima + imi)) / 2;
        for (int k = 0; k < p; ++k)
        {
          s->seq[i * s->period + k * q + j] -= o;
          s->seq[i * s->period + k * q + j] /= sc;
        }
        for (int k = 0; k < p; ++k)
        {
          double _Complex x = s->seq[i * s->period + k * q + j];
          x = cabs(x) + I * carg(x);
          s->seq[i * s->period + k * q + j] = x;
        }
        rma = -1.0/0.0, rmi = 1.0/0.0;
        for (int k = 0; k < p; ++k)
        {
          double _Complex x = s->seq[i * s->period + k * q + j];
          rma = fmax(rma, creal(x));
          rmi = fmin(rmi, creal(x));
        }
        sc = (rma - rmi) / 2;
        sc += sc == 0.0;
        double ro = (rma + rmi) / 2;
        for (int k = 0; k < p; ++k)
        {
          double _Complex x = s->seq[i * s->period + k * q + j];
          x = (creal(x) - ro) / sc + I * cimag(x);
          s->seq[i * s->period + k * q + j] = x;
        }
      }
      p *= s->den[i];
    }

    sample peak = 0;
    for (int i = 0; i < TABSIZE+3; ++i)
    {
      sample sum = 0;
      sample t = i / (sample) TABSIZE * (sample)twopi;
      sample g = 1;
      for (int n = 0; n < 6; ++n)
      {
        sum += g * sin((sample)(n + 1) * t);
        g *= (sample) 0.5;
      }
      s->saw[i] = sum;
      peak = fmax(peak, fabs(sum));
    }
    for (int i = 0; i < TABSIZE+3; ++i)
    {
      s->saw[i] /= peak;
    }
    peak = 0;
    for (int i = 0; i < TABSIZE+3; ++i)
    {
      sample sum = 0;
      sample t = i / (sample) TABSIZE * (sample)twopi;
      for (int n = 1; n < 12; n += 2)
      {
        sum += sin(n * t) / n;
      }
      s->sqr[i] = sum;
      peak = fmax(peak, fabs(sum));
    }
    for (int i = 0; i < TABSIZE+3; ++i)
    {
      s->sqr[i] /= peak;
    }

    s->beat = 0;
    s->phase.phase = 0;
    s->sh.trigger = 0;
    s->sh.value = 0;
  }

  sample mixdown[2] = { 0, 0 };

  sample old = s->phase.phase;
  s->beat = samphold(&s->sh, s->beat + 1, phasor(&s->phase, 4*s->bpm/60));
  if (s->phase.phase < old && s->beat <= s->period)
  {
    for (int i = 0; i < s->depth; ++i)
    {
      double _Complex x = s->seq[i * s->period + (s->beat % s->period)];
      switch (s->preset)
      {
        case 1: TheMilitaryIndustrialComplex_trigger(&s->v1[i], x, i, s->note_offset); break;
        case 2: AfterTheDespoilment_trigger(&s->v2[i], x, i, s->note_offset); break;
        case 3: RaceAgainstTime_trigger(&s->v3[i], x, i, s->note_offset); break;
        case 4: BeforeCivilisation_trigger(&s->v4[i], x, i, s->note_offset); break;
      }
    }
  }

  for (int i = 0; i < s->depth; ++i)
  {
    switch (s->preset)
    {
      case 1: TheMilitaryIndustrialComplex_go(&s->v1[i], &mixdown[0]); break;
      case 2: AfterTheDespoilment_go(&s->v2[i], &mixdown[0], &s->saw[0]); break;
      case 3: RaceAgainstTime_go(&s->v3[i], &mixdown[0], &s->sqr[0]); break;
      case 4: BeforeCivilisation_go(&s->v4[i], &mixdown[0], &s->saw[0]); break;
    }
  }

  const sample gain1[5] = { 0,  0.24,  0.75,  1.00, 12.00 };
  const sample gain2[5] = { 0,  2.00,  2.00,  2.00,  1.00 };
  sample rms = 0;
  for (int c = 0; c < 2 && c < outchannels; ++c)
  {
    out[c] = fmin(fmax(round((sample)32766 * gain2[s->preset] * tanh(gain1[s->preset] * mixdown[c] / sqrt((sample)s->depth)) + (rand()/(sample)RAND_MAX - (sample)0.5)), (sample)-32768.), (sample)32767.);
    sample o = out[c]/(sample)32768.;
    rms += o * o;
    s->peak = fmax(s->peak, fabs(o));
  }
  s->sum0 += 2;
  s->sum2 += rms;
  rms = lop(&s->rms, rms, 10);

  s->reloaded = 0;
  return s->period < s->beat && ! (rms > (sample) 1e-6);
}

S state;

short buffer[SR][2];

static inline sample dbfs(sample x)
{
  return 10 * log10(x);
}

int main(int argc, char **argv)
{
  memset(&state, 0, sizeof(state));
  srand(time(0));

  const char *preset = getenv("ZZC_PRESET");
  if (preset && 0 <= atoi(preset) && atoi(preset) <= 4)
  {
    state.preset = atoi(preset);
  }
  else
  {
    state.preset = 1 + (rand() % 4);
  }
  state.bpm = exp(mix(log(85), log(170), rand() / (sample) RAND_MAX));
  state.note_offset = 0;
  for (int i = 0; i < 12; ++i)
  {
    state.note_offset += rand() / (sample) RAND_MAX - (sample) 0.5;
  }

  if (argc > 1)
  {
    state.depth = argc - 1;
    for (int i = 0; i < state.depth && i < MAXDEPTH; ++i)
    {
      sscanf(argv[i + 1], "%d/%d", &state.num[i], &state.den[i]);
    }
  }
  else
  {
    int period = 0;
    do
    {
      state.depth = ceil(pow(rand() / (sample) RAND_MAX, 2) * MAXDEPTH);
      period = 1;
      for (int i = 0; i < state.depth && i < MAXDEPTH; ++i)
      {
        state.den[i] = round(exp(mix(log(MINDEN), log(MAXDEN), pow(rand() / (sample) RAND_MAX, 2))));
        if (MINDEN <= state.den[i] && state.den[i] <= MAXDEN)
        {
          do
          {
            state.num[i] = 1 + (rand() % (state.den[i] - 1));
          }
          while (gcd(state.num[i], state.den[i]) != 1);
          period *= state.den[i];
        }
        else
        {
          period = 0;
          break;
        }
      }
    }
    while (! (MINPERIOD <= period && period <= MAXPERIOD && MINDEPTH <= state.depth && state.depth <= MAXDEPTH));
  }

  const char *names[] =
    { "(none)"
    , "The Military-Industrial Complex"
    , "After The Despoilment"
    , "Race Against Time"
    , "Before Civilisation"
    };
  printf("%s\n%.3fbpm %+.3fcents\n", names[state.preset], state.bpm, state.note_offset * 100);
  for (int i = 0; i < state.depth; ++i)
  {
    printf("%d/%d%c", state.num[i], state.den[i], i == state.depth - 1 ? '\n' : ' ');
  }

  state.reloaded = 1;
  SF_INFO info = { 0, SR, 2, SF_FORMAT_WAV | SF_FORMAT_PCM_16, 0, 0 };
  SNDFILE *ofile = state.preset ? sf_open("zzc.wav", SFM_WRITE, &info) : 0;
  if (! ofile && state.preset)
  {
    fprintf(stderr, "error: could not open zzc.wav for writing\n");
    return 1;
  }

  int quit = 0;
  int64_t progress = 0;
  while (! quit)
  {
    int i;
    for (i = 0; ! quit && i < SR; ++i)
    {
      sample estimated = SR * state.period / (4 * state.bpm / 60);
      int pc = (progress+1) * 100 / estimated;
      int pc_old = progress * 100 / estimated;
      if (pc != pc_old)
      {
        fprintf(stderr, "%3d%%\r", pc);
      }
      quit = go(&state, 2, &buffer[i][0]);
      progress++;
    }
    if (ofile) sf_writef_short(ofile, &buffer[0][0], i);
  }

  if (ofile) sf_close(ofile);

  sample rms = sqrt(state.sum2 / state.sum0);
  printf("peak %.3f (%.3f dB) rms %.3f (%.3f dB)\n", state.peak, dbfs(state.peak), rms, dbfs(rms));
  return 0;
}
