# z->z^2+c

C99 port of zzc (originally implemented in Pd+GridFlow).

## Build

- <https://code.mathr.co.uk/mandelbrot-numerics>

  installed into `~/opt`

- libsndfile, gcc, make, pkg-config, mpv, imagemagick, ...

  use your distribution's packages

Then run `make`.

## Run

```
LD_LIBRARY_PATH=${HOME}/opt/lib ./zzc 1/2 2/3 3/4 4/5
```

Output in `zzc.wav`.

To render an image too:

```
LD_LIBRARY_PATH=${HOME}/opt/lib ZZC_JULIA=640x360x4 ./zzc 1/2 2/3 3/4 4/5
```

Output in `zzc.ppm` (640x360 with 4 samples per pixel)
and `zzc.wav`.

To choose a preset, set the environment variable `ZZC_PRESET=n`
(valid values: 1, 2, 3, 4).

Run without arguments for a random location.

## Player

For gallery exhibition / installation:

```
./zzc-player.sh
```

Edit `etc/zzc-player` for configuration.
See `etc/zzc-player.sample` for example.

Requires `mpv-0.35.0` or newer (Debian Bookworm).
To backport to Debian Bullseye:

```
sudo apt update
sudo apt install meson
sudo apt build-dep mpv
# install build-deps manually if necessary
# add backports deb to /etc/apt/sources.list
# add testing deb + deb-src to /etc/apt/sources.list
sudo apt update
sudo apt install meson/bullseye-backports
# as regular user
apt source mpv/testing
cd mpv-*

nano debian/control
- libffmpeg-nvenc-dev [amd64 arm64 i386],
- libplacebo-dev (>= 4.157),
- libzimg-dev,

nano debian/rules
+override_dh_shlibdeps:
+       dh_shlibdeps -a -Xmpv

debuild -i -us -uc -b
cd ..
sudo dpkg -i *.deb
```

Note that these debs will be
missing proper dependency information.

## Radio

For streaming internet radio

```
./zzc-streamer.sh
```

Edit `etc/zzc-streamer` for configuration.
See `etc/zzc-streamer.sample` for example.
`AUDIOSIZE` should be at least 100MB below
the space available in the tmp directory.
Also needs configuration in `etc/darkice.cfg` and `etc/netrc`.

```
sudo adduser zzc
sudo adduser zzc audio
sudo modprobe snd-aloop
echo "snd-aloop" | sudo tee -a /etc/modules
# check that the alsa loopback device is card 2
# note: may become card 0 after reboot
sudo su - zzc -c "aplay -l"
sudo su - zzc -c "arecord -l"
# if not you will need to edit asoundrc and darkice.cfg
sudo apt install git build-essential make libmpc-dev mpv darkice curl
sudo su - zzc
mkdir -p code/code.mathr.co.uk
cd code/code.mathr.co.uk
git clone https://code.mathr.co.uk/mandelbrot-numerics.git
git clone https://code.mathr.co.uk/zzc.git
make -C mandelbrot-numerics/c/lib install
make -C zzc/src
cp zzc/src/etc/darkice.cfg.sample zzc/src/etc/darkice.cfg
chmod 600 zzc/src/etc/darkice.cfg
cp zzc/src/etc/netrc.sample zzc/src/etc/netrc
chmod 600 zzc/src/etc/netrc
# edit zzc/src/etc/darkice.cfg and zzc/src/etc/netrc to add credentials
cp zzc/src/etc/asoundrc ~/.asoundrc
# append zzc/src/etc/fstab to system fstab
# copy zzc/src/etc/security/limits.d/audio.conf to system
crontab -e
@reboot /home/zzc/code/code.mathr.co.uk/zzc/src/zzc-streamer.sh
# edit zzc/src/etc/zzc-streamer to configure
# reboot, or start script manually for testing
# hard stop:
sudo killall -u zzc
```

Tested on rpi3.

## Legal

zzc.c -- z->z^2+c - Mandelbrot Music

Copyright (C) 2010,2018,2023 Claude Heiland-Allen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

---
<https://mathr.co.uk>
