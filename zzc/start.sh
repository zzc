#!/bin/sh
pd \
  -nrt \
  -jack -channels 2 -r 48000 \
  -path gridflow/abstractions \
  -helppath gridflow/doc/flow_classes \
  -lib vcf~:zexy:Gem:gridflow \
  -stderr -verbose \
  -open main.pd
